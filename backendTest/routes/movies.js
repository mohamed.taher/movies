const {listMoviesController} = require('../controllers/movie');
const express = require('express');
const router = express.Router();

router.get('/listMovies', listMoviesController)

module.exports = router; 
