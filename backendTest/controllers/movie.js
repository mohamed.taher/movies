const MovieArr = require("../models/movie")
const _ = require('lodash');


//list movies controller
exports.listMoviesController = (req, res) =>
  res.status(200).send({
    data: MovieArr,
    status: 200
  });