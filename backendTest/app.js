const express = require('express');
const app = express();
const http = require("http").Server(app)

require('./startup/logging')();
require('./startup/routes')(app);

const port = process.env.PORT || 5000;
http.listen(port, () => console.log(`Listening on port ${port}...`));