const error = require('../middleware/error');
const movies = require('../routes/movies');


const cors = require('cors');
const bodyParser = require('body-parser');


module.exports = function (app) {
  app.use(cors());

  app.use(bodyParser.json({limit: '100mb'}));
  app.use(bodyParser.urlencoded({
    limit: '100mb',
    extended: true
  }));

  app.use('/api/movies', movies)

  app.use(error);
} 
