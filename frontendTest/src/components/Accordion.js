import React from 'react';
import Collapse, { Panel } from 'rc-collapse';
import 'rc-collapse/assets/index.css'


export default ({data=[]}) => {
    return (
        <div>
            <Collapse accordion={true}>
                {data.map((item, ind) => {
                    return (
                        <Panel key={ind} header={item.title} headerClass="my-header-class">
                            {item.body}
                        </Panel>
                    );
                })}
            </Collapse>
        </div>
    );
}