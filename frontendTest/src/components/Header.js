import React from 'react';

const Header = ({
  title="",
  viewsCount="",
}) => {

    return (
      <div className="header">
          <div className="title">{title}</div>
          <div className="viewsCount">({viewsCount} - views)</div>
      </div>
    );
}

export default Header