import React, { useEffect, useState } from 'react';
import Accordion from "./Accordion"
import Header from "./Header"
import Body from "./Body"

import axios from 'axios'

const Container = () => {
  let [accArr, setAccordArr] = useState([])

  useEffect(() => {(async () => {

      let url = "http://localhost:5000/api/movies/listMovies",
        res = await axios.get(url)
      let arr = res.data.data;

      let AccoordionArr = arr.map(m => {
        let {description, title, createdAt, viewsCount} = m
        return ({
          title: <Header {...{viewsCount,title}} />,
          body: <Body {...{description,createdAt}} />
        })
      })

      setAccordArr(AccoordionArr)

    })()});



  return (
    <div className="container">
      <div className="acc">
      <Accordion data={accArr} />
      </div>
    </div>
  );
}



export default Container