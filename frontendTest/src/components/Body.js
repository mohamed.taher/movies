import React from 'react';

const Body = ({
  description="",
  createdAt
}) => {

    return (
      <div>        
        <div className="body">{description}</div>
        <div className="createdAt">{createdAt}</div>
      </div>
      
    );
}

export default Body